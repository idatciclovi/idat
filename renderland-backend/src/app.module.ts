import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import environment from './api/v1/environment/environment';
import { MongooseModule } from '@nestjs/mongoose';
import { MongooseDatabase } from './api/v1/database/mongoose.database';
import { UserModule } from './api/v1/rest/user/user.module';
import { AuthModule } from './api/v1/rest/auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      load: [environment],
      isGlobal: true,
      ignoreEnvFile: false,
      ignoreEnvVars: false,
    }),
    MongooseModule.forRootAsync({
      useClass: MongooseDatabase,
    }),
    UserModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
