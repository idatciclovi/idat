import { DynamicModule, ForwardReference, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  MongooseModuleOptions,
  MongooseOptionsFactory,
} from '@nestjs/mongoose';

@Injectable()
export class MongooseDatabase implements MongooseOptionsFactory {
  constructor(private readonly _config: ConfigService) {}

  createMongooseOptions(): MongooseModuleOptions {
    const { name, user, pwd, host, port } =
      this._config.getOrThrow<any>('database');
    return {
      uri: `mongodb://${host}:${port}`,
      dbName: name,
      auth: {
        username: user,
        password: pwd,
      },
    };
  }
}
