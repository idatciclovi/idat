import { Prop } from '@nestjs/mongoose';

export class BaseEntity {
  @Prop({
    name: '__create_date__',
    default: new Date(),
  })
  createDate: string;

  @Prop({
    name: '__update_date__',
    default: new Date(),
  })
  updateDate: string;

  @Prop({
    name: '__workspace_create__',
  })
  workspaceCreate: string;

  @Prop({
    name: '__workspace_update__',
  })
  workspaceUpdate: string;

  @Prop({
    name: '__ip_request__',
  })
  ipRequest: string;

  @Prop({
    name: '__deleted__',
  })
  deleted: boolean;
}
