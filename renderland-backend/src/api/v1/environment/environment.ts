export default () => ({
  database: {
    name: process.env.LOCAL_DATABASE_NAME,
    user: process.env.LOCAL_DATABASE_USER,
    pwd: process.env.LOCAL_DATABASE_PWD,
    host: process.env.LOCAL_DATABASE_HOST,
    port: +process.env.LOCAL_DATABASE_PORT,
  },
  jwt: {
    secret: process.env.LOCAL_JWT_SECRET,
    issuer: process.env.LOCAL_JWT_ISSUER,
  },
  host: process.env.LOCAL_PROGRAM_HOST,
});
