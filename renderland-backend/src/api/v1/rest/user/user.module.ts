import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { USER_ENTITY_SCHEMA, UserEntity } from './schema/user.entity';
import { UserModel } from './model/user.model';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: UserEntity.name, schema: USER_ENTITY_SCHEMA },
    ]),
  ],
  controllers: [UserController],
  providers: [UserService, UserModel],
})
export class UserModule {}
