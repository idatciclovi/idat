import { Injectable } from '@nestjs/common';
import { UserModel } from './model/user.model';

@Injectable()
export class UserService {
  constructor(private readonly _model: UserModel) {}

  findById(userId: string) {
    return this._model.findById(userId);
  }

  create() {
    return this._model.create();
  }
}
