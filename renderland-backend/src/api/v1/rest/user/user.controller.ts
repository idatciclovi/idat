import { Controller, Get, Post, Req, Res, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { Request, Response } from 'express';
import { JwtGuard } from '../auth/guards/jwt.guard';

@Controller('user')
@UseGuards(JwtGuard)
export class UserController {
  constructor(private readonly _service: UserService) {}

  @Get('profile')
  profile(@Req() req: Request, @Res() res: Response) {
    const { userId } = req.user as any;
    this._service.findById(userId).subscribe((result) => {
      res.status(200).send(result);
    });
  }

  @Post('register')
  register(@Req() req: Request, @Res() res: Response) {
    this._service.create().subscribe((result) => {
      res.status(200).send(result);
    });
  }
}
