import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { UserEntity } from '../schema/user.entity';
import { Observable } from 'rxjs';
import { IResponse } from '../../../interface/response.interface';
import { v4 as uuid } from 'uuid';
import { hashSync } from 'bcrypt';

@Injectable()
export class UserModel {
  constructor(
    @InjectModel(UserEntity.name) private readonly _model: Model<UserEntity>,
  ) {}

  findByEmail(emailOrUsername: string) {
    const isEmail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(
      emailOrUsername,
    );
    const prop = isEmail ? 'email' : 'username';
    return new Observable<UserEntity>((observer) => {
      try {
        this._model
          .findOne(
            {
              [prop]: emailOrUsername,
            },
            {
              _id: true,
              email: true,
              name: true,
              username: true,
              lastName: true,
              password: true,
              likes: true,
              savesLater: true,
            },
          )
          .exec()
          .then((result) => {
            observer.next(result);
            observer.complete();
          });
      } catch (ex: any) {
        console.log(ex.message);
        observer.next();
        observer.complete();
      }
    });
  }

  findById(userId: string) {
    const res: IResponse<UserEntity> = {
      statusCode: 200,
      message: [],
      body: [],
    };
    return new Observable<IResponse<UserEntity>>((observer) => {
      try {
        this._model
          .findById(userId)
          .select([
            '_id',
            'name',
            'lastName',
            'username',
            'email',
            'likes',
            'savesLater',
          ])
          .then((data) => {
            res.body = [data];
            observer.next(res);
            observer.complete();
          });
      } catch (ex: any) {
        console.log(ex.message);
        res.statusCode = 500;
        res.message = [
          {
            error_code: 'ERROR',
            message: ex.message,
          },
        ];
        observer.next(res);
        observer.complete();
      }
    });
  }

  create() {
    return new Observable<UserEntity>((observer) => {
      try {
        this._model
          .create({
            _id: uuid(),
            name: 'Darwin David',
            lastName: 'Castro Astudillo',
            likes: [],
            email: 'd.castro@codeglobal.co',
            password: hashSync('password', 12),
            savesLater: [],
            template: {
              createDate: new Date(),
              workspaceCreate: 'backend',
              updateDate: new Date(),
              workspaceUpdate: 'backend',
              ipRequest: '::1',
              deleted: false,
            },
            username: 'dcastro',
          })
          .then((data) => {
            observer.next(data);
            observer.complete();
          });
      } catch (ex: any) {
        console.log(ex.message);
        observer.next();
        observer.complete();
      }
    });
  }
}
