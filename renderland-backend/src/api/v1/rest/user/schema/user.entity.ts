import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { BaseEntity } from '../../../base/entity/base.entity';
import { HydratedDocument } from 'mongoose';

export type UserDocument = HydratedDocument<UserEntity>;

@Schema({
  collection: 'user',
})
export class UserEntity {
  @Prop({
    required: true,
    _id: true,
  })
  _id: string;

  @Prop({
    type: 'string',
  })
  name: string;

  @Prop({
    type: 'string',
    alias: 'last_name',
  })
  lastName: string;

  @Prop({
    unique: true,
    required: true,
  })
  username: string;

  @Prop({
    type: 'string',
    unique: true,
    required: true,
  })
  email: string;

  @Prop()
  password: string;

  @Prop()
  likes: string[];

  @Prop({
    alias: 'save_later',
  })
  savesLater: string[];

  @Prop(() => BaseEntity)
  template: BaseEntity;
}

export const USER_ENTITY_SCHEMA = SchemaFactory.createForClass(UserEntity);
