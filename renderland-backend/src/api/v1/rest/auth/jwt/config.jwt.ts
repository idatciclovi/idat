import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModuleOptions, JwtOptionsFactory } from '@nestjs/jwt';

@Injectable()
export class ConfigJwt implements JwtOptionsFactory {
  constructor(private readonly _config: ConfigService) {}

  createJwtOptions(): JwtModuleOptions {
    const { secret, issuer } = this._config.getOrThrow('jwt');
    return {
      secret,
      global: true,
      signOptions: {
        algorithm: 'HS384',
        issuer,
        audience: issuer,
      },
      verifyOptions: {
        maxAge: '7d',
        issuer,
        ignoreExpiration: false,
        audience: issuer,
      },
    };
  }
}
