import { Injectable } from '@nestjs/common';
import { UserModel } from '../user/model/user.model';
import { Observable } from 'rxjs';
import { UserEntity } from '../user/schema/user.entity';
import { IResponse } from '../../interface/response.interface';
import { Error } from 'mongoose';
import { JwtService } from '@nestjs/jwt';
import { compareSync } from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private readonly _model: UserModel,
    private readonly _jwt: JwtService,
  ) {}

  login(login: { username: string; password: string }) {
    const res: IResponse<{ user: UserEntity; token: string }> = {
      statusCode: 200,
      body: [],
      message: [],
    };
    return new Observable<IResponse<{ user: UserEntity; token: string }>>(
      (observer) => {
        this._model.findByEmail(login.username).subscribe((user) => {
          try {
            if (!user) {
              throw new Error('Unauthorized');
            }

            if (!compareSync(login.password, user.password)) {
              throw new Error('Unauthorized');
            }

            const token = this._jwt.sign({
              userId: user._id,
              username: user.username,
            });

            user.password = undefined;

            res.body = [
              {
                user: user,
                token,
              },
            ];

            observer.next(res);
            observer.complete();
          } catch (ex: any) {
            res.statusCode = 401;
            res.message = [
              {
                error_code: 'UNAUTHORIZED',
                message: 'username or password is incorrect',
              },
            ];
            observer.next(res);
            observer.complete();
          }
        });
      },
    );
  }
}
