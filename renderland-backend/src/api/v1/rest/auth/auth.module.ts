import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { ConfigJwt } from './jwt/config.jwt';
import { JwtStrategy } from './strategies/jwt.strategy';
import { MongooseModule } from '@nestjs/mongoose';
import { USER_ENTITY_SCHEMA, UserEntity } from '../user/schema/user.entity';
import { UserModel } from '../user/model/user.model';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: UserEntity.name,
        schema: USER_ENTITY_SCHEMA,
      },
    ]),
    JwtModule.registerAsync({
      global: false,
      useClass: ConfigJwt,
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, UserModel],
})
export class AuthModule {}
