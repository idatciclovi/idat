import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly _config: ConfigService) {
    const { secret, issuer } = _config.getOrThrow('jwt');
    super({
      issuer: issuer,
      audience: issuer,
      ignoreExpiration: false,
      secretOrKey: secret,
      algorithms: 'HS384',
      jsonWebTokenOptions: {
        maxAge: '7d',
      },
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    });
  }

  validate(payload: any) {
    return payload;
  }
}
