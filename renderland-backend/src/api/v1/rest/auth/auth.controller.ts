import { Body, Controller, Get, Post, Res, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Response } from 'express';
import { JwtGuard } from './guards/jwt.guard';

@Controller('auth')
export class AuthController {
  constructor(private readonly _service: AuthService) {}

  @Post('login')
  login(
    @Res() res: Response,
    @Body() body: { username: string; password: string },
  ) {
    this._service.login(body).subscribe((result) => {
      res.status(200).send(result);
    });
  }
}
