export class IResponse<T> {
  statusCode: number;
  message: { error_code: string; message: string }[];
  body: T[];
}
