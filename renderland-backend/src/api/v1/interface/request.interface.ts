export interface IRequest<T> {
  body: T[];
  ipRequest: string;
  workspace: string;
}
