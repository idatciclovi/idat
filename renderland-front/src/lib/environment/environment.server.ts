import { SECRET_RENDERLAND_HOST_API } from '$env/static/private';
import { PUBLIC_RENDERLAND_HOST_APP } from '$env/static/public';

export const ENVIRONMENT_SERVER = {
	host_api: SECRET_RENDERLAND_HOST_API,
	host_app: PUBLIC_RENDERLAND_HOST_APP
};
